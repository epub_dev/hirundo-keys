#!/bin/env ruby

require 'yaml'

key_file = YAML.load_file 'authorized_keys.yaml'

authorized_keys_file = ''

key_file['authorized_keys'].each do |key|
  authorized_keys_file << key['key']
end

File.write('authorized_keys', authorized_keys_file)
