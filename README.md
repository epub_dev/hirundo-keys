# Hirundo user public keys

This is the canonical location for the hirundo deployment user's authorized public SSH keys

A Jenkins job will run the generate_authorized_keys.rb file when it notices changes to this repo, and then update the hirundo users' authorized_keys file. 

Users should add thier public key to the authorized_keys.yaml file and commit the key

